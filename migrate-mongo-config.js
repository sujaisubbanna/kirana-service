const ecosystem = require('../ecosystem.config');

const db = ecosystem.apps.find((x) => x.name === 'skipyq-service').env;

const config = {
  mongodb: {
    url: db.MONGO_URL,
    databaseName: 'skipyq',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  migrationsDir: 'migrations',
  changelogCollectionName: 'changelog',
  migrationFileExtension: '.js',
};

module.exports = config;
