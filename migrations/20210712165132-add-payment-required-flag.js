module.exports = {
  async up(db, client) {
    const storesCursor = db.collection('stores').find();
    for (
      let doc = await storesCursor.next();
      doc != null;
      doc = await storesCursor.next()
    ) {
      await db.collection('stores').updateOne(
        { _id: doc._id },
        {
          $set: {
            paymentRequired: false,
          },
        },
      );
    }

  },

  async down(db, client) {
    await db
      .collection('stores')
      .updateMany({}, { $unset: { paymentRequired: 1 } });
  },
};
