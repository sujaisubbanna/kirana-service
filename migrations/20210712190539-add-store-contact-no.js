module.exports = {
  async up(db, client) {
    const ownersCursor = db.collection('owners').find();
    for (
      let doc = await ownersCursor.next();
      doc != null;
      doc = await ownersCursor.next()
    ) {
      const identity = await db.collection('identities').findOne({ _id: doc.identity });
      await Promise.all(doc.stores.map(async (x) => {
        await db.collection('stores').updateOne(
          { _id: x },
          {
            $set: {
              contactNo: identity.mobile,
            },
          },
        );
      }));
    }
  },

  async down(db, client) {
    await db
      .collection('stores')
      .updateMany({}, { $unset: { contactNo: 1 } });
  },
};
