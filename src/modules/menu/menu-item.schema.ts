import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ versionKey: false, _id: false, timestamps: false })
export class MenuItem extends Document {

}

export const MenuItemSchema = SchemaFactory.createForClass(MenuItem);
