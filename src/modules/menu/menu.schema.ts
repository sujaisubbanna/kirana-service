import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Store } from '../store/store.schema';
import { MenuItem } from './menu-item.schema';

@Schema({ timestamps: true, versionKey: false })
export class Menu extends Document {
  _id: string;


  @Prop({ type: Types.ObjectId, ref: 'Store', required: true })
  store: Store | string;

//   @Prop({ type: [{ type: MenuItem }] })
//   items: MenuItem[];

  createdAt: Date;
  updatedAt: Date;
}

export const MenuSchema = SchemaFactory.createForClass(Menu);
