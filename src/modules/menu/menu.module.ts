import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { CustomerModule } from '../customer/customer.module';
import { NotificationModule } from '../notification/notification.module';
import { StoreModule } from '../store/store.module';
import { MenuService } from './menu.service';
import { MenuSchema } from './menu.schema';
import { MenuController } from './menu.controller';

@Module({
  providers: [MenuService],
  controllers: [MenuController],
  exports: [MenuService],
  imports: [
    forwardRef(() => CustomerModule),
    forwardRef(() => StoreModule),
    NotificationModule,
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Menu',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (config: ConfigService): Schema => {
            const schema = MenuSchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class MenuModule {}
