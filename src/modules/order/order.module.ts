import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema, createConnection } from 'mongoose';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { CustomerModule } from '../customer/customer.module';
import { NotificationModule } from '../notification/notification.module';
import { StoreModule } from '../store/store.module';
import { OrderController } from './order.controller';
import { OrderSchema } from './order.schema';
import { OrderService } from './order.service';
import * as AutoIncrementFactory from 'mongoose-sequence';

@Module({
  providers: [OrderService],
  controllers: [OrderController],
  exports: [OrderService],
  imports: [
    forwardRef(() => CustomerModule),
    forwardRef(() => StoreModule),
    NotificationModule,
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Order',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (config: ConfigService): Schema => {
            const schema = OrderSchema;
            schema.plugin(MongooseAutopopulate);
            const connection = createConnection(
              config.get<string>('MONGO_URL'),
              { useNewUrlParser: true, useUnifiedTopology: true },
            );
            const AutoIncrement = AutoIncrementFactory(connection);
            schema.plugin(AutoIncrement, {
              id: 'orderSeq',
              inc_field: 'orderNo',
              reference_fields: ['store'],
            });
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class OrderModule {}
