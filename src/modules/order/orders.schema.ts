import { Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { OrderType } from 'src/shared/enums/order-type.enum';
import { Order } from './order.schema';

@Schema({ timestamps: false, versionKey: false, _id: false })
export class Orders extends Document {
  @Prop({ type: [{ type: Types.ObjectId, ref: 'Order' }], default: [] })
  [OrderType.OPEN]: string[] | Order[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Order' }], default: [] })
  [OrderType.COMPLETED]: string[] | Order[];

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Order' }], default: [] })
  [OrderType.CANCELLED]: string[] | Order[];
}
