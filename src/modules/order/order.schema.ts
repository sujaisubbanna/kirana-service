import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { OrderStatus } from 'src/shared/enums/order-status.enum';
import { PaymentStatus } from 'src/shared/enums/payment-status.enum';
import { Item } from 'src/shared/models/item.model';
import { Customer } from '../customer/customer.schema';
import { Store } from '../store/store.schema';

@Schema({ timestamps: true, versionKey: false })
export class Order extends Document {
  _id: string;

  orderNo: string;

  @Prop({ type: [{ type: Object }], default: [] })
  items: Item[];

  @Prop({ type: Number, default: 0.0 })
  grossAmount: number;

  @Prop({ type: Number, default: 0.0 })
  netAmount: number;

  @Prop({ type: Number, default: 0.0 })
  discount: number;

  @Prop({ type: OrderStatus, required: true, default: OrderStatus.CREATED })
  status: OrderStatus;

  @Prop({ type: PaymentStatus, required: true, default: PaymentStatus.UNPAID })
  paymentStatus: PaymentStatus;

  @Prop({ type: Types.ObjectId, ref: 'Customer', required: true })
  customer: Customer | string;

  @Prop({ type: Types.ObjectId, ref: 'Store', required: true })
  store: Store | string;

  @Prop({ type: String, default: null })
  notes: string;

  createdAt: Date;
  updatedAt: Date;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
