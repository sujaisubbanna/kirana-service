import { ApiProperty } from '@nestjs/swagger';
import { Item } from 'src/shared/models/item.model';

export class OrderDTO {
  @ApiProperty()
  items: Item[];

  @ApiProperty()
  store: string;

  @ApiProperty()
  notes: string;

  customer?: string;
}
