import {
  Controller,
  Get,
  Param,
  Delete,
  UseGuards,
  Query,
  HttpException,
  HttpStatus,
  Post,
  Body,
} from '@nestjs/common';
import { CurrentCustomerFromJWT } from 'src/shared/decorators/current-customer.decorator';
import { CurrentOwnerFromJWT } from 'src/shared/decorators/current-store-owner.decorator';
import { Roles } from 'src/shared/decorators/role.decorator';
import { OrderType } from 'src/shared/enums/order-type.enum';
import { Role } from 'src/shared/enums/role.enum';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { JwtRoleGuard } from 'src/shared/guards/role.guard';
import { Item } from 'src/shared/models/item.model';
import { Customer } from '../customer/customer.schema';
import { Owner } from '../owner/owner.schema';
import { StoreService } from '../store/store.service';
import { OrderDTO } from './dto/order.dto';
import { OrderService } from './order.service';

@Controller('order')
export class OrderController {
  constructor(
    private orderService: OrderService,
    private storeService: StoreService,
  ) { }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post()
  async create(
    @CurrentCustomerFromJWT() currentCustomerFromJWT: Customer,
    @Body() orderDTO: OrderDTO,
  ) {
    orderDTO.customer = currentCustomerFromJWT._id;
    return await this.orderService.create(orderDTO);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get()
  getAll() {
    return this.orderService.findAll();
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('store/:id')
  async getOrdersForStore(
    @CurrentOwnerFromJWT() currentOwnerFromJWT: Owner,
    @Param('id') storeId: string,
    @Query() query,
  ) {
    const orderType = query['orderType'] as OrderType;
    const store = await this.storeService.findOne(storeId);
    const cursor = query['cursor'];
    const limit = query['limit'];

    if (store.owner.toString() === currentOwnerFromJWT._id.toString()) {
      return this.orderService.findAllForStore(
        storeId,
        orderType,
        cursor,
        +limit,
      );
    } else {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('customer')
  async getOrdersForCustomer(
    @CurrentCustomerFromJWT() currentCustomerFromJWT: Customer,
    @Query() query,
  ) {
    const orderType = query['orderType'] as OrderType;
    const cursor = query['cursor'];
    const limit = query['limit'];
    return this.orderService.findAllForCustomer(
      currentCustomerFromJWT._id,
      orderType,
      cursor,
      +limit,
    );
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('customer/recent')
  async getRecentOrdersForCustomer(
    @CurrentCustomerFromJWT() currentCustomerFromJWT: Customer,
    @Query() query,
  ) {
    const cursor = query['cursor'];
    const limit = query['limit'];
    return this.orderService.findRecentForCustomer(
      currentCustomerFromJWT._id,
      cursor,
      +limit,
    );
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('customer/:id')
  async getOrderForCustomer(
    @CurrentCustomerFromJWT() currentCustomerFromJWT: Customer,
    @Param('id') id: string,
  ) {
    return this.orderService.findOneForCustomer(currentCustomerFromJWT, id);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('/owner/store/:id')
  async getOrderForOwner(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
  ) {
    return this.orderService.findOneForOwner(currentOwner, id);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('/cancel/:id')
  async cancelOrder(@Param('id') id: string) {
    return this.orderService.cancelOrder(id);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('/complete/:id')
  async completeOrder(@Param('id') id: string) {
    return this.orderService.completeOrder(id);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('/accept/:id')
  async acceptOrder(
    @Param('id') id: string,
    @Body() body: { items: Item[]; discount: number }
  ) {
    return this.orderService.acceptOrder(id, body?.items, body?.discount);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('status/:id')
  async setOrderPacked(
    @Param('id') id: string,
    @Body() body: { items: Item[]; discount: number },
  ) {
    return this.orderService.setOrderPacked(
      id,
      body?.items,
      body?.discount,
    );
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.orderService.findOne(id);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Delete(':id')
  deleteOne(@Param('id') id: string) {
    this.orderService.delete(id);
    return null;
  }
}
