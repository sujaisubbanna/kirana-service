import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { OrderStatus } from 'src/shared/enums/order-status.enum';
import { OrderType } from 'src/shared/enums/order-type.enum';
import { PaymentStatus } from 'src/shared/enums/payment-status.enum';
import { TimePeriod } from 'src/shared/enums/time-period.enum';
import { Item } from 'src/shared/models/item.model';
import { getStartDate } from 'src/shared/utils';
import { Customer } from '../customer/customer.schema';
import { CustomerService } from '../customer/customer.service';
import { NotificationService } from '../notification/notification.service';
import { Owner } from '../owner/owner.schema';
import { StoreService } from '../store/store.service';
import { OrderDTO } from './dto/order.dto';
import { Order } from './order.schema';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel('Order') private orderModel: Model<Order>,
    private storeService: StoreService,
    private customerService: CustomerService,
    private notificationService: NotificationService,
  ) { }

  async create(orderDTO: OrderDTO): Promise<Order> {
    const store = await this.storeService.findOne(orderDTO.store);
    if (store.open) {
      const newOrder = new this.orderModel({
        items: orderDTO.items,
        notes: orderDTO.notes,
        customer: orderDTO.customer,
        store: Types.ObjectId(orderDTO.store),
        status: OrderStatus.CREATED,
        paymentStatus: PaymentStatus.UNPAID,
      });
      const order = await newOrder.save();

      (store.orders[OrderType.OPEN] as string[]).push(order._id);
      store.markModified('orders');
      await store.save();

      const customer = await this.customerService.findOne(orderDTO.customer);
      (customer.orders[OrderType.OPEN] as string[]).push(order._id);
      customer.markModified('orders');
      await customer.save();

      order.store = store;
      await this.notificationService.orderCreated(
        store.owner as string,
        customer.name,
        store.name,
      );
      return order;
    } else {
      throw new HttpException('Store closed', HttpStatus.BAD_REQUEST);
    }
  }

  async findCount(timePeriod: TimePeriod): Promise<number> {
    const date = getStartDate(timePeriod);
    return this.orderModel.countDocuments({
      createdAt: { $gt: date },
    });
  }

  async findAll(): Promise<Order[]> {
    return this.orderModel.find();
  }

  async cancelOrder(orderId: string) {
    const order = await this.orderModel.findById(orderId);
    const customer = await this.customerService.findOne(
      order.customer as string,
    );
    const store = await this.storeService.findOne(order.store as string);

    order.status = OrderStatus.CANCELLED;
    await order.save();

    customer.orders.OPEN = (customer.orders.OPEN as string[]).filter(
      (x) => x === orderId,
    );
    (customer.orders.CANCELLED as string[]).push(orderId);
    customer.markModified('orders');
    await customer.save();

    store.orders.OPEN = (store.orders.OPEN as string[]).filter(
      (x) => x === orderId,
    );
    (store.orders.CANCELLED as string[]).push(orderId);
    store.markModified('orders');
    await this.notificationService.orderCancelled(
      order.customer as string,
      store.name,
    );
    await store.save();
    order.customer = customer;
    return order;
  }

  async completeOrder(orderId: string) {
    const order = await this.orderModel.findById(orderId);
    const customer = await this.customerService.findOne(
      order.customer as string,
    );
    const store = await this.storeService.findOne(order.store as string);

    order.status = OrderStatus.COMPLETED;
    order.paymentStatus = PaymentStatus.PAID;
    await order.save();

    customer.orders.OPEN = (customer.orders.OPEN as string[]).filter(
      (x) => x === orderId,
    );
    (customer.orders.COMPLETED as string[]).push(orderId);
    customer.markModified('orders');
    await customer.save();

    store.orders.OPEN = (store.orders.OPEN as string[]).filter(
      (x) => x === orderId,
    );
    (store.orders.COMPLETED as string[]).push(orderId);
    store.markModified('orders');
    await store.save();
    order.customer = customer;
    return order;
  }

  async setOrderPacked(
    orderId: string,
    items: Item[],
    discount: number,
  ) {
    const order = await this.orderModel.findById(orderId);
    const store = await this.storeService.findOne(order.store as string);
    if (store.paymentRequired) {
      order.status = OrderStatus.PACKED;
      order.paymentStatus = PaymentStatus.PAID;
      order.markModified('status');
      order.markModified('paymentStatus');
      await this.notificationService.orderPacked(
        order.customer as string,
        store.name,
      );
      return await order.populate('customer').execPopulate();
    } else {
      let amount = 0;
      items.forEach((e) => {
        amount += e.price;
      });
      order.grossAmount = amount;
      order.discount = discount;
      order.netAmount = amount - discount;
      order.status = OrderStatus.PACKED;
      order.items = items;
      order.markModified('items');
      order.markModified('status');
      order.markModified('amount');
      await this.notificationService.orderPacked(
        order.customer as string,
        store.name,
      );
      await order.save();
      return await order.populate('customer').execPopulate();
    }
  }

  async acceptOrder(orderId: string, items: Item[], discount: number) {
    const order = await this.orderModel.findById(orderId);
    const store = await this.storeService.findOne(order.store as string);
    if (store.paymentRequired) {
      let amount = 0;
      items.forEach((e) => {
        amount += e.price;
      });
      order.grossAmount = amount;
      order.discount = discount;
      order.netAmount = amount - discount;
      order.status = OrderStatus.AWAITING_PAYMENT;
      order.items = items;
      order.markModified('items');
      order.markModified('status');
      order.markModified('amount');
      await this.notificationService.orderPacked(
        order.customer as string,
        store.name,
      );
      await order.save();
      return await order.populate('customer').execPopulate();
    } else {
      order.status = OrderStatus.ACCEPTED;
      order.markModified('status');
      await this.notificationService.orderAccepted(
        order.customer as string,
        store.name,
      );
      await order.save();
      return await order.populate('customer').execPopulate();
    }
  }

  async findAllForStore(
    storeId: string,
    orderType: OrderType,
    cursor: string,
    limit: number,
  ): Promise<Order[]> {
    let options = {};
    if (cursor) {
      options = {
        _id: { $lt: cursor },
      };
    }

    options['store'] = Types.ObjectId(storeId);

    options['status'] = {
      $in: this._getOrderStatus(orderType),
    };

    return this.orderModel
      .find(options)
      .sort({ createdAt: -1, updatedAt: -1 })
      .limit(limit)
      .populate('customer');
  }

  async findAllForCustomer(
    customerId: string,
    orderType: OrderType,
    cursor: string,
    limit: number,
  ): Promise<Order[]> {
    let options = {};
    if (cursor) {
      options = {
        _id: { $lt: cursor },
      };
    }

    options['customer'] = customerId;

    options['status'] = {
      $in: this._getOrderStatus(orderType),
    };

    return this.orderModel
      .find(options)
      .sort({ createdAt: -1, updatedAt: -1 })
      .limit(limit)
      .populate('store');
  }

  async findRecentForCustomer(
    customerId: string,
    cursor: string,
    limit: number,
  ): Promise<Order[]> {
    let options = {};
    if (cursor) {
      options = {
        _id: { $lt: cursor },
      };
    }

    options['customer'] = customerId;

    return this.orderModel
      .find(options)
      .sort({ updatedAt: -1 })
      .limit(limit)
      .populate('store');
  }

  _getOrderStatus(orderType: OrderType) {
    switch (orderType) {
      case OrderType.OPEN:
        return [OrderStatus.CREATED, OrderStatus.ACCEPTED, OrderStatus.PACKED, OrderStatus.AWAITING_PAYMENT];
      case OrderType.COMPLETED:
        return [OrderStatus.COMPLETED];
      case OrderType.CANCELLED:
        return [OrderStatus.CANCELLED];
    }
  }

  async findOneForCustomer(currentCustomer: Customer, id: string) {
    const order = await this.orderModel.findById(id).populate('store');
    if (order.customer.toString() === currentCustomer._id.toString()) {
      return order;
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async findOneForOwner(currentOwner: Owner, id: string) {
    const order = await this.orderModel.findById(id).populate('customer');
    if ((currentOwner.stores as string[]).includes(order.store.toString())) {
      return order;
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async findOne(id: string): Promise<Order> {
    return this.orderModel.findById(id);
  }

  async delete(id: string): Promise<Order> {
    return this.orderModel.findByIdAndRemove(id);
  }
}
