import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Role } from 'src/shared/enums/role.enum';

@Schema({ timestamps: true, versionKey: false })
export class Identity extends Document {
  _id: string;

  @Prop({
    enum: Object.keys(Role),
    index: true,
    default: Role.CUSTOMER,
  })
  role: Role;

  @Prop({ type: String, default: null })
  pushToken: string;

  @Prop({ type: Boolean, default: false })
  verified: boolean;

  @Prop({ type: String, required: true, unique: true, length: 10 })
  mobile: string;

  createdAt: Date;
  updatedAt: Date;
}

export const IdentitySchema = SchemaFactory.createForClass(Identity);
