import { Controller, Get, Param, Delete } from '@nestjs/common';
import { IdentityService } from './identity.service';

@Controller('identity')
export class IdentityController {
  constructor(private readonly identityService: IdentityService) {}

  @Get()
  getAll() {
    return this.identityService.findAll();
  }

  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.identityService.findOne(id);
  }

  @Delete(':id')
  deleteOne(@Param('id') id: string) {
    this.identityService.delete(id);
    return null;
  }
}
