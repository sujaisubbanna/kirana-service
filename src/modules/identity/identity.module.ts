import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import { IdentitySchema } from './identity.schema';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { IdentityService } from './identity.service';
import { IdentityController } from './identity.controller';

@Module({
  providers: [IdentityService],
  controllers: [IdentityController],
  exports: [IdentityService],
  imports: [
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Identity',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (): Schema => {
            const schema = IdentitySchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class IdentityModule {}
