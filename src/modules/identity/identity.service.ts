import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role } from 'src/shared/enums/role.enum';
import { Identity } from './identity.schema';

@Injectable()
export class IdentityService {
  constructor(
    @InjectModel('Identity') private identityModel: Model<Identity>,
  ) {}

  async create(role: Role, mobile: string): Promise<Identity> {
    const newIdentity = new this.identityModel({
      role,
      mobile,
    });
    return await newIdentity.save();
  }

  async findAll(): Promise<Identity[]> {
    return this.identityModel.find();
  }

  async findByMobile(mobile: string): Promise<Identity> {
    return this.identityModel.findOne({
      mobile,
    });
  }

  async findOne(id: string): Promise<Identity> {
    return this.identityModel.findById(id);
  }

  async delete(id: string): Promise<Identity> {
    return this.identityModel.findByIdAndRemove(id);
  }
}
