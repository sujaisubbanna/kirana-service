import {
  Controller,
  Get,
  Param,
  Delete,
  UseGuards,
  Post,
  UseInterceptors,
  Body,
  Query,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Roles } from 'src/shared/decorators/role.decorator';
import { Role } from 'src/shared/enums/role.enum';
import { TimePeriod } from 'src/shared/enums/time-period.enum';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { JwtRoleGuard } from 'src/shared/guards/role.guard';
import { HTTPAPIKeyInterceptor } from 'src/shared/interceptors/http-api-key.interceptor';
import { CustomerService } from '../customer/customer.service';
import { OrderService } from '../order/order.service';
import { OwnerService } from '../owner/owner.service';
import { StoreService } from '../store/store.service';
import { AdminService } from './admin.service';
import { AdminDTO } from './dto/admin.dto';

@Controller('admin')
export class AdminController {
  constructor(
    private adminService: AdminService,
    private orderService: OrderService,
    private customerService: CustomerService,
    private storeService: StoreService,
    private ownerService: OwnerService,
  ) {}

  @Post()
  @UseInterceptors(HTTPAPIKeyInterceptor)
  async create(@Body() adminDTO: AdminDTO) {
    return await this.adminService.create(adminDTO);
  }

  @Get('stats')
  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  async getStats(@Query() query) {
    const timePeriod = TimePeriod[query['timePeriod']];
    if (timePeriod !== undefined) {
      const data = {
        customers: await this.customerService.findCount(timePeriod),
        orders: await this.orderService.findCount(timePeriod),
        stores: await this.storeService.findCount(timePeriod),
        owners: await this.ownerService.findCount(timePeriod),
      };
      return data;
    } else {
      throw new HttpException('Invalid time period', HttpStatus.BAD_REQUEST);
    }
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get()
  getAll() {
    return this.adminService.findAll();
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.adminService.findOne(id);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Delete(':id')
  deleteOne(@Param('id') id: string) {
    this.adminService.delete(id);
    return null;
  }
}
