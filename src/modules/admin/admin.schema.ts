import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Identity } from '../identity/identity.schema';

@Schema({ timestamps: true, versionKey: false })
export class Admin extends Document {
  _id: string;

  @Prop({ type: Types.ObjectId, ref: 'Identity', required: true })
  identity: Identity | string;

  @Prop({ type: String, required: true })
  name: string;

  createdAt: Date;
  updatedAt: Date;
}

export const AdminSchema = SchemaFactory.createForClass(Admin);
