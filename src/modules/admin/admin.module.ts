import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import { AdminSchema } from './admin.schema';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { IdentityModule } from '../identity/identity.module';
import { OrderModule } from '../order/order.module';
import { StoreModule } from '../store/store.module';
import { CustomerModule } from '../customer/customer.module';
import { OwnerModule } from '../owner/owner.module';

@Module({
  providers: [AdminService],
  controllers: [AdminController],
  exports: [AdminService],
  imports: [
    forwardRef(() => OrderModule),
    forwardRef(() => StoreModule),
    forwardRef(() => CustomerModule),
    forwardRef(() => IdentityModule),
    forwardRef(() => OwnerModule),
    ConfigModule,
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Admin',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (): Schema => {
            const schema = AdminSchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class AdminModule {}
