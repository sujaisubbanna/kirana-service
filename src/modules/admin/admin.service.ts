import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { Role } from 'src/shared/enums/role.enum';
import { IdentityService } from '../identity/identity.service';
import { Admin } from './admin.schema';
import { AdminDTO } from './dto/admin.dto';

@Injectable()
export class AdminService {
  constructor(
    @InjectModel('Admin') private adminModel: Model<Admin>,
    private identityService: IdentityService,
  ) {}

  async create(adminDTO: AdminDTO): Promise<Admin> {
    const identity = await this.identityService.create(
      Role.ADMIN,
      adminDTO.mobile,
    );
    const newAdmin = new this.adminModel({
      identity: identity._id,
      name: adminDTO.name,
    });
    return await newAdmin.save();
  }

  async findAll(): Promise<Admin[]> {
    return this.adminModel.find();
  }

  async findOne(id: string): Promise<Admin> {
    return this.adminModel.findById(id);
  }

  async findByIdentity(id: string): Promise<Admin> {
    return this.adminModel
      .findOne({ identity: id } as FilterQuery<Admin>)
      .populate('identity');
  }

  async delete(id: string): Promise<Admin> {
    return this.adminModel.findByIdAndRemove(id);
  }
}
