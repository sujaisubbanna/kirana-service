import { ApiProperty } from '@nestjs/swagger';

export class AdminDTO {
  @ApiProperty()
  name: string;

  @ApiProperty()
  mobile: string;
}
