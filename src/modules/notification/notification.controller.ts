import { Controller, Post } from '@nestjs/common';
import { NotificationService } from './notification.service';

@Controller('notification')
export class NotificationController {
  constructor(private notificationService: NotificationService) {}
  @Post()
  async sample() {
    await this.notificationService.sendPushNotification({
      tokens: [
        'ci_fIw2eQcy5Gbc1ETnfBs:APA91bG0rQ2nY1rFneJR5S_yhFBxxYvv5y6fa3hXjv8vVXABptn5DaXLgw1v5UG2QYOpiyvpIAwQ_e3GcQMZTfefTk6Qv0IE8xRR_AZA3ICLzHFm68wL4fnE5or_EGm4FeKWrQBbl7Ew',
      ],
      title: 'test',
      body: 'hey',
      data: {},
    });
  }
}
