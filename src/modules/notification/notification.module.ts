import { Module } from '@nestjs/common';
import { CustomerModule } from '../customer/customer.module';
import { OwnerModule } from '../owner/owner.module';
import { NotificationService } from './notification.service';
import { NotificationController } from './notification.controller';

@Module({
  providers: [NotificationService],
  exports: [NotificationService],
  imports: [OwnerModule, CustomerModule],
  controllers: [NotificationController],
})
export class NotificationModule {}
