/* eslint-disable prettier/prettier */
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as firebaseAdmin from 'firebase-admin';
import { CustomerService } from '../customer/customer.service';
import { Identity } from '../identity/identity.schema';
import { OwnerService } from '../owner/owner.service';
import { PushNotificationPayloadDTO } from './dto/push-notification-payload.dto';

@Injectable()
export class NotificationService {
  constructor(
    private ownerService: OwnerService,
    private customerService: CustomerService,
  ) {}

  async sendPushNotification(
    pushNotificationPayloadDTO: PushNotificationPayloadDTO,
  ): Promise<any> {
    try {
      const pushNotificationPayload: firebaseAdmin.messaging.MulticastMessage =
        {
          tokens: pushNotificationPayloadDTO.tokens,
          notification: {
            title: pushNotificationPayloadDTO.title,
            body: pushNotificationPayloadDTO.body,
          },
          data: pushNotificationPayloadDTO.data,
        };
      return await firebaseAdmin
        .messaging()
        .sendMulticast(pushNotificationPayload);
    } catch (error) {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

  async orderCreated(ownerId: string, customerName: string, storeName: string) {
    const owner = await this.ownerService.findOneWithIdentity(ownerId);
    const token = (owner.identity as Identity).pushToken;
    if (token !== null && token !== undefined) {
      await this.sendPushNotification({
        tokens: [token],
        title: 'New Order!',
        body: `${customerName} has placed a new order at ${storeName}.`,
        data: {},
      });
    }
  }

  async orderAccepted(customerId: string, storeName: string) {
    const customer = await this.customerService.findOneWithIdentity(customerId);
    const token = (customer.identity as Identity).pushToken;
    if (token !== null && token !== undefined) {
      await this.sendPushNotification({
        tokens: [token],
        title: 'Order Accepted!',
        body: `Your order at ${storeName} has been accepted`,
        data: {},
      });
    }
  }

  async orderPacked(customerId: string, storeName: string) {
    const customer = await this.customerService.findOneWithIdentity(customerId);
    const token = (customer.identity as Identity).pushToken;
    if (token !== null && token !== undefined) {
      await this.sendPushNotification({
        tokens: [token],
        title: 'Order Packed!',
        body: `Your order at ${storeName} has been packed and is ready for pick up.`,
        data: {},
      });
    }
  }

  async orderCancelled(customerId: string, storeName: string) {
    const customer = await this.customerService.findOneWithIdentity(customerId);
    const token = (customer.identity as Identity).pushToken;
    if (token !== null && token !== undefined) {
      await this.sendPushNotification({
        tokens: [token],
        title: 'Order Cancelled!',
        body: `Your order at ${storeName} has been cancelled.`,
        data: {},
      });
    }
  }
}
