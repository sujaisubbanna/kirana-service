import { IsArray, IsNotEmpty } from 'class-validator';

export class PushNotificationPayloadDTO {
  constructor(tokens: string[], title: string, body: string, data: any) {
    this.tokens = tokens;
    this.title = title;
    this.body = body;
    this.data = data;
  }

  @IsArray()
  tokens: string[];

  @IsNotEmpty()
  body: string;

  @IsNotEmpty()
  title: string;

  data: any;
}
