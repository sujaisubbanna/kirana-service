import { ApiProperty } from '@nestjs/swagger';
import { LocationModel } from 'src/shared/models/location.model';

export class UpdateCustomerDTO {
  @ApiProperty()
  name?: string;

  @ApiProperty()
  location?: LocationModel;
}
