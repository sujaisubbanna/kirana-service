import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { DISCORD_WEBHOOK_URL } from 'src/shared/constants';
import { Role } from 'src/shared/enums/role.enum';
import { TimePeriod } from 'src/shared/enums/time-period.enum';
import { LocationModel } from 'src/shared/models/location.model';
import { getStartDate } from 'src/shared/utils';
import { IdentityService } from '../identity/identity.service';
import { Customer } from './customer.schema';

@Injectable()
export class CustomerService {
  constructor(
    @InjectModel('Customer') private customerModel: Model<Customer>,
    private identityService: IdentityService,
    private http: HttpService,
  ) { }

  async create(mobile: string): Promise<Customer> {
    const identity = await this.identityService.create(Role.CUSTOMER, mobile);
    const newCustomer = new this.customerModel({
      identity: identity._id,
    });
    return newCustomer.save();
  }

  async signUp(
    id: string,
    name: string,
    address: LocationModel,
  ): Promise<Customer> {
    const customer = await this.customerModel.findById(id);
    customer.name = name;
    customer.location = address;
    await customer.save();
    return customer;
  }

  async feedback(body: string): Promise<void> {
    await this.http.post(DISCORD_WEBHOOK_URL, { content: body }).toPromise();
  }

  async update(
    id: string,
    name: string,
    address: LocationModel,
  ): Promise<Customer> {
    const customer = await this.customerModel.findById(id);
    customer.name = name;
    customer.location = address;
    await customer.save();
    return customer;
  }

  async findCount(timePeriod: TimePeriod): Promise<number> {
    const date = getStartDate(timePeriod);
    return this.customerModel.countDocuments({
      createdAt: { $gt: date },
    });
  }

  async findAll(): Promise<Customer[]> {
    return this.customerModel.find();
  }

  async findOne(id: string): Promise<Customer> {
    return this.customerModel.findById(id);
  }

  async findOneWithIdentity(id: string): Promise<Customer> {
    return this.customerModel.findById(id).populate('identity');
  }

  async findByIdentity(id: string): Promise<Customer> {
    return this.customerModel
      .findOne({
        identity: id,
      } as FilterQuery<Customer>)
      .populate('identity');
  }

  async delete(id: string): Promise<Customer> {
    return this.customerModel.findByIdAndRemove(id);
  }
}
