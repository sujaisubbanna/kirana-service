import {
  Controller,
  Get,
  Param,
  Delete,
  UseGuards,
  Post,
  Body,
} from '@nestjs/common';
import { CurrentCustomerFromJWT } from 'src/shared/decorators/current-customer.decorator';
import { Roles } from 'src/shared/decorators/role.decorator';
import { Role } from 'src/shared/enums/role.enum';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { JwtRoleGuard } from 'src/shared/guards/role.guard';
import { Customer } from './customer.schema';
import { CustomerService } from './customer.service';
import { UpdateCustomerDTO } from './dto/update-customer.dto';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) { }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get()
  async getAll() {
    return await this.customerService.findAll();
  }

  @Roles(Role.CUSTOMER, Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('feedback')
  async feedback(
    @Body() data: { body: string },
  ) {
    return await this.customerService.feedback(
      data.body
    );
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('sign-up')
  async signUp(
    @CurrentCustomerFromJWT() currentCustomer: Customer,
    @Body() data: UpdateCustomerDTO,
  ) {
    return await this.customerService.signUp(
      currentCustomer._id,
      data.name,
      data.location,
    );
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('update')
  async update(
    @CurrentCustomerFromJWT() currentCustomer: Customer,
    @Body() data: UpdateCustomerDTO,
  ) {
    return await this.customerService.update(
      currentCustomer._id,
      data.name,
      data.location,
    );
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get(':id')
  async getOne(@Param('id') id: string) {
    return await this.customerService.findOne(id);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    await this.customerService.delete(id);
    return null;
  }
}
