import { forwardRef, HttpModule, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';
import { CustomerSchema } from './customer.schema';
import { IdentityModule } from '../identity/identity.module';

@Module({
  providers: [CustomerService],
  controllers: [CustomerController],
  exports: [CustomerService],
  imports: [
    forwardRef(() => IdentityModule),
    HttpModule,
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Customer',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (): Schema => {
            const schema = CustomerSchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class CustomerModule {}
