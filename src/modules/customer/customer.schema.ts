import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { OrderType } from 'src/shared/enums/order-type.enum';
import { LocationModel } from 'src/shared/models/location.model';
import { Identity } from '../identity/identity.schema';
import { Orders } from '../order/orders.schema';

@Schema({ timestamps: true, versionKey: false })
export class Customer extends Document {
  _id: string;

  @Prop({ type: String, default: null })
  name: string;

  @Prop({ type: Types.ObjectId, ref: 'Identity', required: true })
  identity: Identity | string;

  @Prop({ type: Object, default: null })
  location: LocationModel;

  @Prop({
    type: Orders,
    default: {
      [OrderType.OPEN]: [],
      [OrderType.COMPLETED]: [],
      [OrderType.CANCELLED]: [],
    },
  })
  orders: Orders;

  createdAt: Date;
  updatedAt: Date;
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);
