import {
  Controller,
  Get,
  Param,
  Delete,
  Post,
  Body,
  UseGuards,
  Query,
} from '@nestjs/common';
import { CurrentCustomerFromJWT } from 'src/shared/decorators/current-customer.decorator';
import { CurrentOwnerFromJWT } from 'src/shared/decorators/current-store-owner.decorator';
import { Roles } from 'src/shared/decorators/role.decorator';
import { Role } from 'src/shared/enums/role.enum';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { JwtRoleGuard } from 'src/shared/guards/role.guard';
import { PaymentDetails } from 'src/shared/models/payment-details.model';
import { PaymentDetailsType } from 'src/shared/models/payment_details_type.enum';
import { Customer } from '../customer/customer.schema';
import { Owner } from '../owner/owner.schema';
import { StoreDTO } from './dto/store.dto';
import { UpdateStoreDTO } from './dto/update-store.dto';
import { StoreService } from './store.service';

@Controller('store')
export class StoreController {
  constructor(private storeService: StoreService) {}

  @Post()
  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  async create(
    @CurrentOwnerFromJWT() owner: Owner,
    @Body() storeDTO: StoreDTO,
  ) {
    storeDTO.owner = owner._id;
    return await this.storeService.create(storeDTO);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get()
  async getAll(@Query() query) {
    const cursor = query['cursor'];
    const limit = query['limit'];
    return await this.storeService.findAll(null, cursor, +limit);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('owner')
  async getAllForOwner(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Query() query,
  ) {
    const cursor = query['cursor'];
    const limit = query['limit'];
    return await this.storeService.findAll(currentOwner._id, cursor, +limit);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('openStatus/:id')
  async toggleOpenStatus(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
  ) {
    await this.storeService.toggleOpen(currentOwner, id);
    return 'OK';
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('paymentRequired/:id')
  async togglePaymentRequired(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
  ) {
    await this.storeService.togglePaymentRequired(currentOwner, id);
    return 'OK';
  }

  // @Roles(Role.OWNER)
  // @UseGuards(JwtAuthGuard, JwtRoleGuard)
  // @Post('openStatus/:id')
  // async toggleMenuAvailability(
  //   @CurrentOwnerFromJWT() currentOwner: Owner,
  //   @Param('id') id: string,
  // ) {
  //   await this.storeService.toggleMenuAvailability(currentOwner, id);
  //   return 'OK';
  // }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('onlinePayment/:id')
  async toggleOnlinePayment(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
  ) {
    await this.storeService.toggleOnlinePayment(currentOwner, id);
    return 'OK';
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('addPaymentDetails/:id/:type')
  async addPaymentDetails(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
    @Param('type') type: PaymentDetailsType,
    @Body() paymentDetails: PaymentDetails,
  ) {
    return await this.storeService.addPaymentDetails(
      currentOwner,
      id,
      paymentDetails,
      type,
    );
  }

  @Roles(Role.CUSTOMER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get('customer')
  async getCustomerStores(
    @CurrentCustomerFromJWT() currentCustomer: Customer,
    @Query() query,
  ) {
    const cursor = query['cursor'];
    const limit = query['limit'];
    return await this.storeService.findCustomerStores(
      currentCustomer,
      cursor,
      limit,
    );
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('deletePaymentDetails/:id/:type')
  async deletePaymentDetails(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
    @Param('type') type: PaymentDetailsType,
  ) {
    return await this.storeService.deletePaymentDetail(currentOwner, id, type);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('update')
  async updateStore(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Body() store: UpdateStoreDTO,
  ) {
    return this.storeService.updateOne(currentOwner, store);
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Delete(':id')
  async deleteOne(
    @CurrentOwnerFromJWT() currentOwner: Owner,
    @Param('id') id: string,
  ) {
    await this.storeService.delete(id, currentOwner._id);
    return 'OK';
  }

  @Roles(Role.OWNER)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Post('image-upload')
  async uploadImage(@Body() body: { image: string }) {
    return await this.storeService.uploadImage(body.image);
  }
}
