import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import { StoreSchema } from './store.schema';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { StoreService } from './store.service';
import { StoreController } from './store.controller';
import { OrderModule } from '../order/order.module';
import { OwnerModule } from '../owner/owner.module';

@Module({
  providers: [StoreService],
  controllers: [StoreController],
  exports: [StoreService],
  imports: [
    ConfigModule,
    forwardRef(() => OrderModule),
    forwardRef(() => OwnerModule),
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Store',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (): Schema => {
            const schema = StoreSchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class StoreModule {}
