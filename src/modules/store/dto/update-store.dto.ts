import { ApiProperty } from '@nestjs/swagger';
import { StoreTag } from 'src/shared/enums/store-tags.enum';
import { StoreType } from 'src/shared/enums/store-type.enum';
import { LocationModel } from 'src/shared/models/location.model';

export class UpdateStoreDTO {
  @ApiProperty()
  id: string;

  @ApiProperty()
  contactNo: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  location: LocationModel;

  @ApiProperty()
  type: StoreType;

  @ApiProperty()
  tags: StoreTag[];

  @ApiProperty()
  image: string;
}
