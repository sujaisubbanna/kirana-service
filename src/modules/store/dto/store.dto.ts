import { ApiProperty } from '@nestjs/swagger';
import { StoreTag } from 'src/shared/enums/store-tags.enum';
import { StoreType } from 'src/shared/enums/store-type.enum';
import { LocationModel } from 'src/shared/models/location.model';

export class StoreDTO {
  @ApiProperty()
  name?: string;

  @ApiProperty()
  contactNo?: string;

  @ApiProperty()
  location?: LocationModel;

  @ApiProperty()
  type?: StoreType;

  @ApiProperty()
  image?: string;

  @ApiProperty()
  tags?: StoreTag[];

  owner?: string;
}
