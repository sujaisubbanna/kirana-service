import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TimePeriod } from 'src/shared/enums/time-period.enum';
import { getStartDate } from 'src/shared/utils';
import { Owner } from '../owner/owner.schema';
import { StoreDTO } from './dto/store.dto';
import { Store } from './store.schema';
import { S3 } from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
import { PutObjectRequest } from 'aws-sdk/clients/s3';
import { ConfigService } from '@nestjs/config';
import { UpdateStoreDTO } from './dto/update-store.dto';
import { PaymentDetails } from 'src/shared/models/payment-details.model';
import { PaymentDetailsType } from 'src/shared/models/payment_details_type.enum';
import { Customer } from '../customer/customer.schema';
import { OwnerService } from '../owner/owner.service';
import * as vision from '@google-cloud/vision';

@Injectable()
export class StoreService {
  constructor(
    @InjectModel('Store') private storeModel: Model<Store>,
    private config: ConfigService,
    private ownerService: OwnerService,
  ) { }

  async findCount(timePeriod: TimePeriod): Promise<number> {
    const date = getStartDate(timePeriod);
    return this.storeModel.countDocuments({
      createdAt: { $gt: date },
    });
  }

  async create(storeDTO: StoreDTO): Promise<Store> {
    const newStore = new this.storeModel({
      name: storeDTO.name,
      location: storeDTO.location,
      type: storeDTO.type,
      contactNo: storeDTO.contactNo,
      image: storeDTO.image,
      owner: storeDTO.owner,
      tags: storeDTO.tags,
    });
    const store = await newStore.save();

    const owner = await this.ownerService.findOne(storeDTO.owner);
    (owner.stores as string[]).push(store._id);
    owner.markModified('stores');
    await owner.save();

    return store;
  }

  async findAll(
    owner: string,
    cursor: string,
    limit: number,
  ): Promise<Store[]> {
    let options = {};
    if (cursor) {
      options = {
        _id: { $lt: cursor },
      };
    }

    if (owner) {
      options['owner'] = owner;
    }

    return await this.storeModel
      .find(options)
      .sort({ createdAt: -1, updatedAt: -1 })
      .limit(limit);
  }

  async findCustomerStores(
    customer: Customer,
    cursor: string,
    limit: number,
  ): Promise<Store[]> {
    let options = {};
    if (cursor) {
      options = {
        _id: { $lt: cursor },
      };
    }

    options['open'] = { $eq: true };
    options['active'] = { $eq: true };

    return await this.storeModel.aggregate([{
      $geoNear: {
        near: { type: 'Point', coordinates: customer.location.geoLocation.coordinates },
        distanceField: 'dist',
        maxDistance: this.config.get<number>('RADIUS_FOR_SEARCH'),
        query: options, 
        spherical: true
      }
    }]).limit(+limit);
  }

  async findOne(id: string): Promise<Store> {
    return this.storeModel.findById(id);
  }

  async toggleOpen(owner: Owner, id: string): Promise<void> {
    const store = await this.storeModel.findById(id);
    if (store.owner.toString() === owner._id.toString()) {
      store.open = !store.open;
      store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async togglePaymentRequired(owner: Owner, id: string): Promise<void> {
    const store = await this.storeModel.findById(id);
    if (store.owner.toString() === owner._id.toString()) {
      store.paymentRequired = !store.paymentRequired;
      if (store.paymentRequired) {
        store.onlinePaymentEnabled = true;
      }
      store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  // async toggleMenuAvailability(owner: Owner, id: string): Promise<void> {
  //   const store = await this.storeModel.findById(id);
  //   if (store.owner.toString() === owner._id.toString()) {
  //     store.menuAvailable = !store.menuAvailable;
  //     store.save();
  //   } else {
  //     throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
  //   }
  // }

  async addPaymentDetails(
    owner: Owner,
    storeId: string,
    paymentDetails: PaymentDetails,
    type: PaymentDetailsType,
  ) {
    const store = await this.storeModel.findById(storeId);
    if (store.owner.toString() === owner._id.toString()) {
      if (store.paymentDetails === null) {
        store.paymentDetails = {
          vpa: null,
          bankAccount: null,
        };
      }
      if (type === PaymentDetailsType.VPA) {
        store.paymentDetails.vpa = paymentDetails.vpa;
      } else {
        store.paymentDetails.bankAccount = paymentDetails.bankAccount;
      }
      store.markModified('paymentDetails');
      return store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async toggleOnlinePayment(owner: Owner, id: string): Promise<void> {
    const store = await this.storeModel.findById(id);
    if (store.owner.toString() === owner._id.toString()) {
      store.onlinePaymentEnabled = !store.onlinePaymentEnabled;
      store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async updateOne(owner: Owner, data: UpdateStoreDTO): Promise<Store> {
    const store = await this.storeModel.findById(data.id);
    if (store.owner.toString() === owner._id.toString()) {
      store.name = data.name;
      store.contactNo = data.contactNo;
      store.image = data.image;
      store.location = data.location;
      store.type = data.type;
      store.tags = data.tags;
      return await store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async deletePaymentDetail(
    owner: Owner,
    id: string,
    type: PaymentDetailsType,
  ) {
    const store = await this.storeModel.findById(id);
    if (owner._id.toString() === store.owner.toString()) {
      switch (type) {
        case PaymentDetailsType.BANK_ACCOUNT:
          store.paymentDetails.bankAccount = null;
          break;
        case PaymentDetailsType.VPA:
          store.paymentDetails.vpa = null;
          break;
      }
      store.markModified('paymentDetails');
      return await store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async delete(id: string, owner: string): Promise<Store> {
    const store = await this.storeModel.findById(id);
    if (store.owner.toString() === owner) {
      store.active = false;
      store.markModified('active');
      return store.save();
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  async checkIfExplicit(data: string) {
    try {
      const client = new vision.ImageAnnotatorClient();
      const response = await client.safeSearchDetection(
        Buffer.from(data.replace(/^data:image\/\w+;base64,/, ''), 'base64'),
      );
      const adult = response[0].safeSearchAnnotation.adult;
      const racy = response[0].safeSearchAnnotation.racy;
      const violence = response[0].safeSearchAnnotation.violence;
      if (
        adult === 'VERY_LIKELY' ||
        racy === 'VERY_LIKELY' ||
        violence === 'VERY_LIKELY' ||
        adult === 'LIKELY' ||
        racy === 'LIKELY' ||
        violence === 'LIKELY'
      ) {
        return true;
      }
      return false;
    } catch (error) {
      console.log(error);
    }
  }

  async uploadImage(image: string) {
    const isExplicit = await this.checkIfExplicit(image);
    if (!isExplicit) {
      const s3 = new S3({
        endpoint: this.config.get('SPACES_ENDPOINT'),
        accessKeyId: this.config.get('SPACES_ACCESS_KEY'),
        secretAccessKey: this.config.get('SPACES_SECRET_ACCESS_KEY'),
      });
      const randomId = uuidv4();
      const buffer = Buffer.from(
        image.replace(/^data:image\/\w+;base64,/, ''),
        'base64',
      );
      const type = image.split(';')[0].split('/')[1];

      const S3params: PutObjectRequest = {
        Bucket: this.config.get('PROFILE_IMAGE_BUCKET'),
        Key: `${randomId}.${type}`,
        Body: buffer,
        ContentEncoding: 'base64',
        ContentType: `image/${type}`,
        ACL: 'public-read',
      };

      return new Promise(async (resolve, reject) => {
        try {
          const { Location } = await s3.upload(S3params).promise();
          return resolve(Location);
        } catch (error) {
          return reject(error);
        }
      });
    } else {
      throw new HttpException(
        'Please choose a different image',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}
