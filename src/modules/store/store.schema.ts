import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { OrderType } from 'src/shared/enums/order-type.enum';
import { StoreTag } from 'src/shared/enums/store-tags.enum';
import { StoreType } from 'src/shared/enums/store-type.enum';
import { LocationModel, LocationSchema } from 'src/shared/models/location.model';
import { PaymentDetails } from 'src/shared/models/payment-details.model';
import { Orders } from '../order/orders.schema';
import { Owner } from '../owner/owner.schema';

@Schema({ timestamps: true, versionKey: false })
export class Store extends Document {
  _id: string;

  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true })
  contactNo: string;

  @Prop({ LocationSchema })
  location: LocationModel;

  @Prop({ type: String, default: null })
  image: string;

  @Prop({ type: Object, default: null })
  paymentDetails: PaymentDetails;

  @Prop({ type: Boolean, default: false })
  onlinePaymentEnabled: boolean;

  @Prop({ type: Boolean, default: false })
  paymentRequired: boolean;
  
  @Prop({
    type: Orders,
    default: {
      [OrderType.OPEN]: [],
      [OrderType.COMPLETED]: [],
      [OrderType.CANCELLED]: [],
    },
  })
  orders: Orders;

  @Prop({ type: Types.ObjectId, ref: 'Owner', required: true })
  owner: Owner | string;

  createdAt: Date;

  updatedAt: Date;

  @Prop({ type: Boolean, default: true })
  active: boolean;

  @Prop({ type: Boolean, default: true })
  open: boolean;

  @Prop({ type: String, required: true })
  type: StoreType;

  @Prop({type: [String], default: []})
  tags: StoreTag[];
}

export const StoreSchema = SchemaFactory.createForClass(Store);
