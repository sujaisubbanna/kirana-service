import { ApiProperty } from '@nestjs/swagger';

export class OwnerDTO {
  @ApiProperty()
  name: string;
  @ApiProperty()
  mobile: string;
}
