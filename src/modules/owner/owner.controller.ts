import {
  Controller,
  Get,
  Param,
  Delete,
  Post,
  Body,
  UseGuards,
} from '@nestjs/common';
import { Roles } from 'src/shared/decorators/role.decorator';
import { Role } from 'src/shared/enums/role.enum';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { JwtRoleGuard } from 'src/shared/guards/role.guard';
import { OwnerDTO } from './dto/owner.dto';
import { OwnerService } from './owner.service';

@Controller('owner')
export class OwnerController {
  constructor(private readonly ownerService: OwnerService) {}

  @Post()
  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  async create(@Body() ownerDTO: OwnerDTO) {
    return await this.ownerService.create(ownerDTO);
  }

  @Get()
  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  getAll() {
    return this.ownerService.findAll();
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Get(':id')
  getOne(@Param('id') id: string) {
    return this.ownerService.findOne(id);
  }

  @Roles(Role.ADMIN)
  @UseGuards(JwtAuthGuard, JwtRoleGuard)
  @Delete(':id')
  deleteOne(@Param('id') id: string) {
    this.ownerService.delete(id);
    return null;
  }
}
