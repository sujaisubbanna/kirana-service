import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { Role } from 'src/shared/enums/role.enum';
import { TimePeriod } from 'src/shared/enums/time-period.enum';
import { getStartDate } from 'src/shared/utils';
import { IdentityService } from '../identity/identity.service';
import { OwnerDTO } from './dto/owner.dto';
import { Owner } from './owner.schema';

@Injectable()
export class OwnerService {
  constructor(
    @InjectModel('Owner') private ownerModel: Model<Owner>,
    private identityService: IdentityService,
  ) {}

  async create(ownerDTO: OwnerDTO): Promise<Owner> {
    const identity = await this.identityService.create(
      Role.OWNER,
      ownerDTO.mobile,
    );
    const newOwner = new this.ownerModel({
      identity: identity._id,
      name: ownerDTO.name,
    });
    return newOwner.save();
  }

  async findCount(timePeriod: TimePeriod): Promise<number> {
    const date = getStartDate(timePeriod);
    return this.ownerModel.countDocuments({
      createdAt: { $gt: date },
    });
  }

  async findAll(): Promise<Owner[]> {
    return this.ownerModel.find().populate('identity');
  }

  async findOne(id: string): Promise<Owner> {
    return this.ownerModel.findById(id);
  }

  async findOneWithIdentity(id: string): Promise<Owner> {
    return this.ownerModel.findById(id).populate('identity');
  }

  async findByIdentity(id: string): Promise<Owner> {
    return this.ownerModel
      .findOne({
        identity: id,
      } as FilterQuery<Owner>)
      .populate('identity');
  }

  async delete(id: string): Promise<Owner> {
    return this.ownerModel.findByIdAndRemove(id);
  }
}
