import { SchemaFactory, Schema, Prop } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Identity } from '../identity/identity.schema';
import { Store } from '../store/store.schema';

@Schema({ timestamps: true, versionKey: false })
export class Owner extends Document {
  _id: string;

  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Types.ObjectId, ref: 'Identity', required: true })
  identity: Identity | string;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Store' }] })
  stores: Store[] | string[];

  createdAt: Date;
  updatedAt: Date;
}

export const OwnerSchema = SchemaFactory.createForClass(Owner);
