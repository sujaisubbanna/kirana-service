import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Schema } from 'mongoose';
import { OwnerSchema } from './owner.schema';
import * as MongooseAutopopulate from 'mongoose-autopopulate';
import { ACTIVE_CONNECTION_NAME } from 'src/shared/constants';
import { OwnerService } from './owner.service';
import { OwnerController } from './owner.controller';
import { IdentityModule } from '../identity/identity.module';

@Module({
  providers: [OwnerService],
  controllers: [OwnerController],
  exports: [OwnerService],
  imports: [
    forwardRef(() => IdentityModule),
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Owner',
          imports: [ConfigModule],
          inject: [ConfigService],
          useFactory: (): Schema => {
            const schema = OwnerSchema;
            schema.plugin(MongooseAutopopulate);
            return schema;
          },
        },
      ],
      ACTIVE_CONNECTION_NAME,
    ),
  ],
})
export class OwnerModule {}
