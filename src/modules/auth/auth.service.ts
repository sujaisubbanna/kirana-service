import { InjectRedis, Redis } from '@nestjs-modules/ioredis';
import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { SEND_OTP_URL } from 'src/shared/constants';
import { JWTPayload } from 'src/shared/models/jwt-payload.model';
import { generateOTP } from 'src/shared/utils';
import { AdminService } from '../admin/admin.service';
import { CustomerService } from '../customer/customer.service';
import { Identity } from '../identity/identity.schema';
import { IdentityService } from '../identity/identity.service';
import { OwnerService } from '../owner/owner.service';
import { LoginDTO } from './dto/login.dto';
import { Role } from 'src/shared/enums/role.enum';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private identityService: IdentityService,
    private adminService: AdminService,
    private customerService: CustomerService,
    private ownerService: OwnerService,
    @InjectRedis() private readonly redis: Redis,
    private config: ConfigService,
    private httpService: HttpService,
  ) {}

  async sendConfirmationCode(mobile: string) {
    const identity = await this.identityService.findByMobile(mobile);
    if (identity === null) {
      await this.customerService.create(mobile);
    }
    const otp = generateOTP();
    await this.redis.set(mobile, otp);
    const url = SEND_OTP_URL(this.config.get('SMS_API_KEY'), mobile, otp);
    await this.httpService.get(url).toPromise();
  }

  async sendConfirmationCodeFake(mobile: string) {
    const identity = await this.identityService.findByMobile(mobile);
    if (identity === null) {
      await this.customerService.create(mobile);
    }
  }

  async updatePushToken(identity: Identity, token: string) {
    identity.pushToken = token;
    await identity.save();
  }

  async verifyConfirmationCode(loginDTO: LoginDTO) {
    const identity = await this.identityService.findByMobile(loginDTO.mobile);
    const otp = await this.redis.get(loginDTO.mobile);
    if (otp === loginDTO.otp) {
      await this.redis.del(loginDTO.mobile);
      identity.verified = true;
      await identity.save();
      return {
        token: this.generateJWTToken(identity),
        data: await this.getUser(identity._id, identity.role),
      };
    }
  }

  async getUser(id: string, role: Role) {
    switch (role) {
      case Role.OWNER:
        return await this.ownerService.findByIdentity(id);
      case Role.CUSTOMER:
        return await this.customerService.findByIdentity(id);
      case Role.ADMIN:
        return await this.adminService.findByIdentity(id);
    }
  }

  async verifyConfirmationCodeFake(loginDTO: LoginDTO) {
    const identity = await this.identityService.findByMobile(loginDTO.mobile);
    identity.verified = true;
    await identity.save();
    return {
      token: this.generateJWTToken(identity),
      data: await this.getUser(identity._id, identity.role),
    };
  }

  async createAuthToken(jwtPayload: JWTPayload): Promise<string> {
    return this.jwtService.sign(JSON.parse(JSON.stringify(jwtPayload)));
  }

  generateJWTToken(identity: Identity): string {
    const payload: JWTPayload = {
      sub: identity._id.toString(),
      role: identity.role,
    };
    return this.jwtService.sign(payload);
  }
}
