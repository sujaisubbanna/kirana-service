import { Prop } from '@nestjs/mongoose';

export class LoginDTO {
  @Prop({ type: String, required: true })
  mobile: string;
  @Prop({ type: String, required: true })
  otp: string;
}
