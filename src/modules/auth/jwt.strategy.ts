import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Role } from 'src/shared/enums/role.enum';
import { JWTPayload } from 'src/shared/models/jwt-payload.model';
import { AdminService } from '../admin/admin.service';
import { IdentityService } from '../identity/identity.service';
import { CustomerService } from '../customer/customer.service';
import { OwnerService } from '../owner/owner.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private config: ConfigService,
    private ownerService: OwnerService,
    private adminService: AdminService,
    private identityService: IdentityService,
    private customerService: CustomerService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('JWT_SECRET'),
    });
  }

  async validate(payload: JWTPayload): Promise<any> {
    const identity = await this.identityService.findOne(payload.sub);
    if (identity.role === Role.ADMIN) {
      return {
        admin: await this.adminService.findByIdentity(identity._id),
      };
    } else if (identity.role === Role.CUSTOMER) {
      return {
        customer: await this.customerService.findByIdentity(identity._id),
      };
    } else if (identity.role === Role.OWNER) {
      return {
        owner: await this.ownerService.findByIdentity(identity._id),
      };
    }
  }
}
