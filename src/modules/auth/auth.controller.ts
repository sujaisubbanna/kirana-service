import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { CurrentAdminFromJWT } from 'src/shared/decorators/current-admin.decorator';
import { CurrentCustomerFromJWT } from 'src/shared/decorators/current-customer.decorator';
import { CurrentOwnerFromJWT } from 'src/shared/decorators/current-store-owner.decorator';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { MethodEnabledInterceptor } from 'src/shared/interceptors/method-enabled-interceptor';
import { Admin } from '../admin/admin.schema';
import { Customer } from '../customer/customer.schema';
import { Identity } from '../identity/identity.schema';
import { Owner } from '../owner/owner.schema';
import { AuthService } from './auth.service';
import { LoginDTO } from './dto/login.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get()
  async sendConfirmationCode(@Query() query) {
    const mobile = query['mobile'];
    await this.authService.sendConfirmationCode(mobile);
    return 'OK';
  }

  @Get('/whoami')
  @UseGuards(JwtAuthGuard)
  async whoAmI(
    @CurrentAdminFromJWT() admin: Admin,
    @CurrentCustomerFromJWT() customer: Customer,
    @CurrentOwnerFromJWT() owner: Owner,
  ) {
    if (admin != null) {
      return admin;
    } else if (customer != null) {
      return customer;
    } else if (owner != null) {
      return owner;
    } else {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }
  }

  @Post('pushToken')
  @UseGuards(JwtAuthGuard)
  async updatePushToken(
    @CurrentAdminFromJWT() admin: Admin,
    @CurrentCustomerFromJWT() customer: Customer,
    @CurrentOwnerFromJWT() owner: Owner,
    @Body() body: { token: string },
  ) {
    let identity: Identity;
    if (admin != null) {
      identity = admin.identity as Identity;
    } else if (customer != null) {
      identity = customer.identity as Identity;
    } else if (owner != null) {
      identity = owner.identity as Identity;
    }
    await this.authService.updatePushToken(identity, body.token);
  }

  @Post()
  async verifyConfirmationCode(@Body() loginDTO: LoginDTO) {
    const { token, data } = await this.authService.verifyConfirmationCode(
      loginDTO,
    );
    if (token !== null) {
      return {
        token,
        data,
      };
    } else {
      throw new HttpException('Wrong OTP', HttpStatus.FORBIDDEN);
    }
  }

  @Get('send')
  @UseInterceptors(MethodEnabledInterceptor)
  async sendConfirmationCodeFake(@Query() query) {
    const mobile = query['mobile'];
    await this.authService.sendConfirmationCodeFake(mobile);
    return 'OK';
  }

  @Post('verify')
  @UseInterceptors(MethodEnabledInterceptor)
  async verifyConfirmationCodeFake(@Body() loginDTO: LoginDTO) {
    const { token, data } = await this.authService.verifyConfirmationCodeFake(
      loginDTO,
    );
    if (token !== null) {
      return {
        token,
        data,
      };
    } else {
      throw new HttpException('Wrong OTP', HttpStatus.FORBIDDEN);
    }
  }
}
