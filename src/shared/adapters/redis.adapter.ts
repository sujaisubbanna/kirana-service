import * as Redis from 'ioredis';

export const redis: Redis.Redis = new Redis({
  port: 6379,
  host: '0.0.0.0',
  family: 4,
});

export const REDIS_TIME_OUT = 60 * 60 * 24;
