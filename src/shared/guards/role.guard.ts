import { Injectable, ExecutionContext, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Identity } from 'src/modules/identity/identity.schema';

@Injectable()
export class JwtRoleGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    const req = context.switchToHttp().getRequest();
    const identity =
      req.user?.customer?.identity ||
      req.user?.owner?.identity ||
      req.user?.admin?.identity;
    const role = (identity as Identity).role;
    const requiredRoles = this.reflector.get<string[]>(
      'roles',
      context.getHandler(),
    );
    return requiredRoles.includes(role);
  }
}
