import {
    Injectable,
    NestInterceptor,
    ExecutionContext,
    CallHandler,
    MethodNotAllowedException,
  } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ConfigService } from '@nestjs/config';
  
  @Injectable()
  export class MethodEnabledInterceptor implements NestInterceptor {
    constructor(private config: ConfigService) {}
  
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
      const flag = this.config.get<string>(
        'NODE_ENV',
      ) !== 'prod';
      if (flag || flag === undefined) return next.handle();
      else throw new MethodNotAllowedException();
    }
  }
  