import { TimePeriod } from '../enums/time-period.enum';
import * as dayjs from 'dayjs';

export const getStartDate = (timePeriod: TimePeriod): Date => {
  switch (timePeriod) {
    case TimePeriod.LAST_24_HRS:
      return dayjs().subtract(1, 'day').toDate();
    case TimePeriod.LAST_WEEK:
      return dayjs().subtract(7, 'day').toDate();
    case TimePeriod.LAST_MONTH:
      return dayjs().subtract(1, 'month').toDate();
    case TimePeriod.ALL_TIME:
      return dayjs().subtract(10, 'year').toDate();
    default:
      return null;
  }
};

export const generateOTP = () => {
  return Math.floor(1000 + Math.random() * 9000).toString();
};
