export const ACTIVE_CONNECTION_NAME = 'active';

export const SEND_OTP_URL = (apiKey, mobile, otp) =>
  `https://2factor.in/API/V1/${apiKey}/SMS/${mobile}/${otp}`;

export const DISCORD_WEBHOOK_URL='https://discord.com/api/webhooks/851406589138042910/wE0daIoyemnms-PYwMcQij-HBFkjN_87QmGNvZEfjz6x0YXoHItU8ozRNvwoP6iVYCVS';