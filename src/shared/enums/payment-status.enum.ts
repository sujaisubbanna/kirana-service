export enum PaymentStatus {
  UNPAID = 'UNPAID',
  PAID = 'PAID',
  CASH = 'CASH',
}
