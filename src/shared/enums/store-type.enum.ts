export enum StoreType {
  MEDICAL = 'MEDICAL',
  GENERAL = 'GENERAL',
  RESTAURANT = 'RESTAURANT',
}
