export enum TimePeriod {
  LAST_24_HRS = 'LAST_24_HRS',
  LAST_WEEK = 'LAST_WEEK',
  LAST_MONTH = 'LAST_MONTH',
  ALL_TIME = 'ALL_TIME',
}
