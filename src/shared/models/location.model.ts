import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export class GeoLocation {
  @Prop({ type: () => String, default: 'Point' })
  type: string;

  @Prop({ type: [Number], default: [], length: 2 })
  coordinates: number[];
}

@Schema({ timestamps: false, versionKey: false })
export class LocationModel extends Document {
  @Prop({ type: String, required: true })
  address: string;

  @Prop({
    type: () => GeoLocation,
    required: true,
    index: {
      type: '2dsphere',
      path: 'geoLocation',
    },
  })
  geoLocation: GeoLocation;
}

export const LocationSchema = raw({
  geoLocation: { type: GeoLocation },
  address: { type: String, text: true },
});
