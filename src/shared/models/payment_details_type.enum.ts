export enum PaymentDetailsType {
  VPA = 'VPA',
  BANK_ACCOUNT = 'BANK_ACCOUNT',
}
