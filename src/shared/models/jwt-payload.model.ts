import { Role } from '../enums/role.enum';

export interface JWTPayload {
  sub: string;
  role: Role;
}
