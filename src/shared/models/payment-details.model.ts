export interface VPA {
  address: string;
}

export interface BankAccount {
  ifsc: string;
  name: string;
  accountNumber: string;
}

export interface PaymentDetails {
  vpa?: VPA;
  bankAccount?: BankAccount;
}
