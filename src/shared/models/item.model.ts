import { Prop, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
@Schema({ timestamps: false, versionKey: false, _id: false })
export class Item extends Document {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Number, default: 0.0 })
  price: number;

  @Prop({ type: String, required: true })
  qty: string;
}
