import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AdminModule } from './modules/admin/admin.module';
import { CustomerModule } from './modules/customer/customer.module';
import { IdentityModule } from './modules/identity/identity.module';
import { OwnerModule } from './modules/owner/owner.module';
import { StoreModule } from './modules/store/store.module';
import { OrderModule } from './modules/order/order.module';
import { AuthModule } from './modules/auth/auth.module';
import { RedisModule } from '@nestjs-modules/ioredis';
import { NotificationModule } from './modules/notification/notification.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        NODE_ENV: Joi.string().required().valid('dev', 'prod').default('dev'),
        PORT: Joi.number().required().default(3000),
        JWT_SECRET: Joi.string().required(),
        API_KEY: Joi.string().required(),
        MONGO_URL: Joi.string()
          .required()
          .default('mongodb://localhost/skipyq'),
        SMS_API_KEY: Joi.string().required(),
        SPACES_ENDPOINT: Joi.string().required(),
        SPACES_ACCESS_KEY: Joi.string().required(),
        SPACES_SECRET_ACCESS_KEY: Joi.string().required(),
        PROFILE_IMAGE_BUCKET: Joi.string().required(),
        GOOGLE_APPLICATION_CREDENTIALS: Joi.string().required(),
        RADIUS_FOR_SEARCH: Joi.number().required().default(5000),
      }),
    }),
    RedisModule.forRoot({
      config: {
        url: 'redis://localhost:6379',
      },
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        uri: config.get<string>('MONGO_URL'),
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      }),
      connectionName: 'active',
    }),
    AuthModule,
    IdentityModule,
    OwnerModule,
    AdminModule,
    CustomerModule,
    StoreModule,
    OrderModule,
    NotificationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
